# cpp-base-image-docker

[![pipeline status](https://gitlab.com/MusicScience37Projects/docker/cpp-base-image-docker/badges/main/pipeline.svg)](https://gitlab.com/MusicScience37Projects/docker/cpp-base-image-docker/-/commits/main)

A base Docker container image of images for CI of C++ projects

The container image built from this project has no compiler.
For images with compilers,
see [gcc-ci-docker](https://gitlab.com/MusicScience37Projects/docker/gcc-ci-docker)
or [clang-ci-docker](https://gitlab.com/MusicScience37Projects/docker/clang-ci-docker).

## Repositories

- [GitLab](https://gitlab.com/MusicScience37Projects/docker/cpp-base-image-docker):
  for development including CI

## Testing

For test of this project,
use `./tool.py test` command.
